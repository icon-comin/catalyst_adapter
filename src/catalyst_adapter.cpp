#include <iostream>
#include <vector>
#include<unistd.h>

#include <comin.h>
#include <catalyst.hpp>

void catalyst_adapter_secondary_constructor();
void catalyst_adapter_execute();
void catalyst_adapter_destruct();
conduit_cpp::Node catalyst_adapter_2d_domain(int jg);

static conduit_cpp::Node catalyst_exec_params;

extern "C"{ // avoid name-mangling
  void comin_main(){
    conduit_cpp::Node catalyst_init_params;
    catalyst_init_params["catalyst/mpi_comm"] = comin_parallel_get_host_mpi_comm();

    const char* script_file = NULL;
    int script_file_len;
    comin_current_get_plugin_options(&script_file, &script_file_len);
    catalyst_init_params["catalyst/scripts/script/filename"] = std::string(script_file, script_file_len);

    catalyst_status err = catalyst_initialize(conduit_cpp::c_node(&catalyst_init_params));
    if (err != catalyst_status_ok) {
      std::cerr << "Failed to initialize Catalyst: " << err << std::endl;
    }

    conduit_cpp::Node about_node;
    err = catalyst_about(conduit_cpp::c_node(&about_node));
    if (err != catalyst_status_ok) {
      std::cerr << "Failed to call catalyst_about: " << err << std::endl;
    }
    about_node.print();

    comin_callback_register(EP_SECONDARY_CONSTRUCTOR, catalyst_adapter_secondary_constructor);
    comin_callback_register(EP_ATM_WRITE_OUTPUT_BEFORE, catalyst_adapter_execute);
    comin_callback_register(EP_DESTRUCTOR, catalyst_adapter_destruct);
  }
}

void catalyst_adapter_secondary_constructor(){
  // prepare the execute node
  for(int jg = 1; jg<=comin_descrdata_get_global_n_dom(); ++jg){
    auto domain_node = catalyst_adapter_2d_domain(jg);
    catalyst_exec_params[std::string("catalyst/channels/2d_domain")+std::to_string(jg)] =
      domain_node;
  }
}

void catalyst_adapter_execute(){

  static int64_t cycle = 0;
  catalyst_exec_params["catalyst/state/cycle"] = cycle;
  static double dt = comin_descrdata_get_timesteplength(1); // TODO jg=1?!
  catalyst_exec_params["catalyst/state/time"] = cycle*dt;
  cycle++;

  // TODO: update pointers of time dependend variables

  enum catalyst_status err = catalyst_execute(conduit_cpp::c_node(&catalyst_exec_params));
  if (err != catalyst_status_ok)
  {
    printf("Failed to execute Catalyst: %d\n", err);
  }
  sleep(1);
}


void catalyst_adapter_destruct(){
  conduit_cpp::Node catalyst_fini_params;
  enum catalyst_status err = catalyst_finalize(conduit_cpp::c_node(&catalyst_fini_params));
  if (err != catalyst_status_ok)
  {
    printf("Failed to execute Catalyst: %d\n", err);
  }
}
