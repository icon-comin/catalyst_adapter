#include <comin.h>
#include <catalyst.hpp>

conduit_cpp::Node catalyst_adapter_2d_domain(int jg){
  conduit_cpp::Node channel;
  // now, create the mesh.
  channel["type"] = "mesh";

  double *vlon, *vlat;
  int nverts = comin_descrdata_get_domain_verts_nverts(jg);
  int nverts_arr[2];
  comin_descrdata_get_domain_verts_vlon(jg, &vlon, nverts_arr);
  comin_descrdata_get_domain_verts_vlat(jg, &vlat, nverts_arr);
  channel["data/coordsets/coords/type"] = "explicit";
  channel["data/coordsets/coords/values/x"].set_external(vlon, nverts);
  channel["data/coordsets/coords/values/y"].set_external(vlat, nverts);
  channel["data/coordsets/coords/values/z"].set(std::vector<double>(nverts, 1.0));

  channel["data/topologies/mesh/type"] = "unstructured";
  channel["data/topologies/mesh/coordset"] = "coords";
  channel["data/topologies/mesh/elements/shape"] = "tri";

  int* vertex_idx = NULL;
  int* vertex_blk = NULL;
  int vertex_idx_size[3];
  int ncells = comin_descrdata_get_domain_cells_ncells(jg);
  comin_descrdata_get_domain_cells_vertex_idx(jg, &vertex_idx, vertex_idx_size);
  comin_descrdata_get_domain_cells_vertex_blk(jg, &vertex_blk, vertex_idx_size);
  int offset = vertex_idx_size[0]*vertex_idx_size[1];
  std::vector<int> vidx(3*ncells);
  for(int c=0; c<ncells; ++c){
    vidx[3*c+0] = (vertex_idx[c+0*offset] - 1) + nverts_arr[0]*(vertex_blk[c+0*offset] - 1);
    vidx[3*c+1] = (vertex_idx[c+1*offset] - 1) + nverts_arr[0]*(vertex_blk[c+1*offset] - 1);
    vidx[3*c+2] = (vertex_idx[c+2*offset] - 1) + nverts_arr[0]*(vertex_blk[c+2*offset] - 1);
  }
  channel["data/topologies/mesh/elements/connectivity"].set(vidx);

  // add all 2d variables
  for(void* it = comin_var_get_descr_list_head();
      it != nullptr;
      it = comin_var_get_descr_list_next(it)){
    t_comin_var_descriptor var_desc;
    comin_var_get_descr_list_var_desc(it, &var_desc);
    int zaxis_id = -1;
    comin_metadata_get_integer(var_desc, "zaxis_id", &zaxis_id);
    if(var_desc.id == jg &&
       zaxis_id == COMIN_ZAXIS_2D){
      channel["data/fields/" + std::string(var_desc.name) + "/association"] = "element";
      channel["data/fields/" + std::string(var_desc.name) + "/topology"] = "mesh";
      channel["data/fields/" + std::string(var_desc.name) + "/volume_dependend"] = false;
      int ep = EP_ATM_WRITE_OUTPUT_BEFORE;
      void* handle = comin_var_get(1, &ep, var_desc, COMIN_FLAG_READ);
      channel["data/fields/" + std::string(var_desc.name) + "/values"]
        .set_external(comin_var_get_ptr(handle), ncells);
    }
  }

  // add some special fields
  channel["data/fields/mpi_rank/association"] = "element";
  channel["data/fields/mpi_rank/topology"] = "mesh";
  channel["data/fields/mpi_rank/volume_dependend"] = false;
  channel["data/fields/mpi_rank/values"]
    .set(std::vector<int>(ncells, comin_parallel_get_host_mpi_rank()));

  channel["data/fields/decomp_domain/association"] = "element";
  channel["data/fields/decomp_domain/topology"] = "mesh";
  channel["data/fields/decomp_domain/volume_dependend"] = false;
  int* decomp_domain_ptr = nullptr;
  int arr_size2[2];
  comin_descrdata_get_domain_cells_decomp_domain(jg, &decomp_domain_ptr, arr_size2);
  channel["data/fields/decomp_domain/values"]
    .set_external(decomp_domain_ptr, ncells);

  return channel;
}
