import os
from paraview.simple import *

# ------------------------------------------------------------------------------
# Catalyst options
from paraview import catalyst
options = catalyst.Options()
options.GlobalTrigger = 'TimeStep'
options.EnableCatalystLive = 1
options.CatalystLiveURL = 'localhost:22222'
options.CatalystLiveTrigger = 'TimeStep'

lonlat2sphere = '-1*sin(coordsX)*cos(coordsY)*iHat+cos(coordsX)*cos(coordsY)*jHat+sin(coordsY)*kHat'

domain1 = TrivialProducer(registrationName='2d_domain1')
calculator1 = Calculator(registrationName='Calculator1', Input=domain1)
calculator1.CoordinateResults = 1
calculator1.Function = lonlat2sphere
calculator1Display = Show(calculator1)

domain2 = TrivialProducer(registrationName='2d_domain2')
calculator2 = Calculator(registrationName='Calculator2', Input=domain2)
calculator2.CoordinateResults = 1
calculator2.Function = lonlat2sphere
calculator2Display = Show(calculator2)

# ------------------------------------------------------------------------------
if __name__ == '__main__':
    from paraview.simple import SaveExtractsUsingCatalystOptions
    # Code for non in-situ environments; if executing in post-processing
    # i.e. non-Catalyst mode, let's generate extracts using Catalyst options
    SaveExtractsUsingCatalystOptions(options)
